#!/bin/bash
echo "Welcome the server configuration file"

file_name=config.yml

if [ -d "config" ]
then
	echo "reading files from the config directory"
	config_files=$(ls config)
else
	echo "the config dir does not exists, please create one"
	mkdir config
fi
echo "We are going to configure the $file_name file"

echo "here are the configuration files: $config_files"
